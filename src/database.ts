import { CommandInteraction, GuildMember, Message, User } from 'discord.js';
import * as sqlite3db from 'sqlite3'
import StatsManager from './stats';
let statment_check = (err: Error) => {
	if (err != null) {
		console.log("statment failed" + err.stack + " " + err.message)
	}

}

const StatMgr = new StatsManager()

export class DatabaseInterface {
	db: sqlite3db.Database;
	constructor() {
		this.db = new sqlite3db.Database("data.db", (err) => {
			if (err != null) {
				console.log("connection to database  faild" + err)
			}
		})
		this.db.run("CREATE TABLE IF NOT EXISTS messages (author  TEXT, content TEXT, attachments TEXT, snowflake TEXT,channel TEXT) ");
		this.db.run("CREATE TABLE IF NOT EXISTS lessons (hour TEXT, room TEXT,subject TEXT, day INT)");
		this.db.run("CREATE TABLE IF NOT EXISTS vc_info (user_id	TEXT,vc_id	TEXT,time	INTEGER)")

	}
	save_message = (mess: Message) => {
		let attachments: string = "";
		mess.attachments.forEach((element) => {
			attachments += element + ";"
		})
		let statment = this.db.prepare("INSERT INTO messages (author,content,attachments,snowflake,channel) VALUES (?,?,?,?,?)", statment_check)
		statment.bind(mess.author.id, mess.content, attachments, mess.id, mess.channel.id);
		statment.run(statment_check).finalize()
	}
	save_vc_info = (vc_info: { dsc_id: string, vc_id: string }[], time: number) => {
		let timestamp: number = new Date().getTime();
		vc_info.forEach((vc_entery) => {
			let stm = this.db.prepare("INSERT INTO vc_info (user_id,vc_id,time)  VALUES (?,?,?)")
			stm.bind(vc_entery.dsc_id, vc_entery.vc_id, timestamp)
			stm.run()
			stm.finalize()

		})



	}
	get_lesson = (time: number[]): Promise<{ hour: string, room: string, subject: string, day: number }> => {
		return new Promise((resolve) => {
			let stm = this.db.prepare("SELECT * FROM lessons WHERE hour = ? AND  day = ?", statment_check);
			let hour = time[0] + ":";
			if (time[1].toString().length == 1) {
				hour += "0" + time[1]
			} else {
				hour += time[1]

			}
			console.log(hour)
			stm.bind(hour, time[2], statment_check);
			stm.run().get((err, row) => {
				resolve(row);
				stm.finalize()

			})
		})

	}
	get_vc_stats = (id: string) => {	
		return new Promise(resolve => {
			let stm = this.db.prepare("SELECT count(user_id) AS 'time', user_id FROM vc_info WHERE user_id = ? GROUP BY user_id", statment_check);
			stm.bind(id)
	
			stm.get(async (err, row) => {			
				resolve(row)
			})
		})	
	}

	get_vc_stats_allusers = () => {		
		return new Promise(resolve => {
			let stm = this.db.prepare("SELECT count(user_id) AS 'time', user_id FROM vc_info GROUP BY user_id ORDER BY time DESC", statment_check);

			stm.all(async (err, row) => {		
				resolve(row)
			})
		}) 
	}
}	
